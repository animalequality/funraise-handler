class $d582dd6eaccab18d$var$AEFunraise {
    static getSettings() {
        return {
            i18n: {
                locale: window.navigator.language,
                phrases: {
                    donation_give: {
                        pt: "Uma",
                        es: "Donar"
                    },
                    donation_frequency_once: {
                        pt: "uma vez",
                        es: "una vez"
                    },
                    donation_give_once: {
                        pt: "Uma vez",
                        es: "Una vez"
                    },
                    donation_ask_amount: {
                        pt: "Escolha um valor para doar",
                        es: "Elige la cantidad a donar"
                    },
                    donation_donate_with: {
                        pt: "Doar com",
                        es: "Contribuir con"
                    },
                    payments_cover_transaction_fees: {
                        pt: "Cobrir as tarifas banc\xe1rias",
                        es: "Cubrir las tarifas bancarias"
                    },
                    payments_cover_fees_message: {
                        pt: "Quando voc\xea aumenta sua doa\xe7\xe3o para cobrir as taxas de transa\xe7\xe3o, est\xe1 ajudando a %{org_name} a utilizar mais de sua doa\xe7\xe3o.",
                        es: "Cuando aumentas tu contribuci\xf3n para cubrir las tarifas de transacci\xf3n, est\xe1s ayudando a %{org_name} a poner m\xe1s de tu aportaci\xf3n en uso."
                    },
                    payments_cover_fees_label: {
                        pt: "Sim, cobrir a tarifa",
                        es: "S\xed, cubrir la tarifa"
                    },
                    donor_information_address_line_2: {
                        pt: "Linha de endere\xe7o 2",
                        es: "L\xednea de direcci\xf3n 2"
                    },
                    payments_credit_card_information_title: {
                        pt: "Informa\xe7\xf5es do cart\xe3o",
                        es: "Informaci\xf3n de la tarjeta"
                    },
                    payments_pay_total: {
                        pt: "Pagar",
                        es: "Pagar"
                    },
                    payments_thank_you_message_1: {
                        pt: "Seu pagamento foi processado com sucesso.",
                        es: "Tu pago se ha procesado correctamente."
                    },
                    payments_thank_you_message_2: {
                        pt: "Voc\xea receber\xe1 um recibo por e-mail em breve",
                        es: "En breve recibir\xe1s un recibo por correo electr\xf3nico"
                    },
                    payments_credit_card_expired_error: {
                        pt: "Seu cart\xe3o expirou",
                        es: "Tu tarjeta est\xe1 vencida"
                    },
                    donation_optional_donation: {
                        pt: "Adicionar uma doa\xe7\xe3o volunt\xe1ria",
                        es: "A\xf1adir una aportaci\xf3n voluntaria"
                    },
                    donation_select_amount: {
                        pt: "Selecione o valor a ser doado",
                        es: "Selecciona el monto del obsequio en pesos mexicanos"
                    },
                    donation_or_enter_amount: {
                        pt: "ou escreva um valor",
                        es: "o ingresa el monto en pesos mexicanos"
                    },
                    donation_allocation_donate_to: {
                        pt: "Doar para",
                        es: "Donar a"
                    },
                    donation_frequency_name: {
                        pt: "Frequ\xeancia",
                        es: "Frecuencia"
                    },
                    donation_frequency_one_time: {
                        pt: "Uma \xfanica vez",
                        es: "Por \xfanica vez"
                    },
                    donation_frequency_weekly: {
                        pt: "Semanal",
                        es: "Semanal"
                    },
                    donation_frequency_monthly: {
                        pt: "Mensalmente",
                        es: "Mensualmente"
                    },
                    donation_frequency_quarterly: {
                        pt: "Trimestral",
                        es: "Trimestral"
                    },
                    donation_frequency_yearly: {
                        pt: "Anual",
                        es: "Anual"
                    },
                    donation_ask_amount_once: {
                        pt: "Contribua uma vez",
                        es: "Contribuye una vez"
                    },
                    donation_ask_amount_monthly: {
                        pt: "Contribua mensalmente",
                        es: "Contribuye mensualmente"
                    },
                    donation_your: {
                        pt: "Sua doa\xe7\xe3o",
                        es: "Tu obsequio"
                    },
                    donation_amount: {
                        pt: "Quantia",
                        es: "Monto"
                    },
                    donation_remove_amount: {
                        pt: "Retirar",
                        es: "Quitar"
                    },
                    donation_options: {
                        es: "Opciones de donaci\xf3n",
                        pt: "Op\xe7\xf5es de donativos"
                    },
                    donation_min_validation: {
                        pt: "O valor deve ser pelo menos de %{currency_symbol}%{amount}",
                        es: "La cantidad debe ser al menos %{currency_symbol}%{amount} pesos"
                    },
                    donation_max_validation: {
                        pt: "O valor deve ser de no m\xe1ximo %{currency_symbol}%{amount}",
                        es: "Cantidad puede ser m\xe1ximo %{currency_symbol}%{amount} pesos"
                    },
                    customize_question_this: {
                        pt: "Esta pergunta",
                        es: "Esta pregunta"
                    },
                    customize_question_select_an_option: {
                        pt: "Selecione uma op\xe7\xe3o",
                        es: "Selecciona una opci\xf3n"
                    },
                    customize_dedication_in_honor_label: {
                        pt: "Em honra de",
                        es: "En honor a"
                    },
                    customize_dedication_in_memory_label: {
                        pt: "Em mem\xf3ria de",
                        es: "En memoria de"
                    },
                    customize_dedication_inspired_by_label: {
                        pt: "Inspirado por",
                        es: "Inspirado por"
                    },
                    customize_dedication_type_hint: {
                        pt: "Selecione um tipo de dedicat\xf3ria",
                        es: "Seleccione un tipo de dedicatoria"
                    },
                    customize_dedication_type_label: {
                        pt: "Tipo de dedicat\xf3ria",
                        es: "Tipo de dedicatoria"
                    },
                    customize_dedication_person_name_label: {
                        pt: "Nome da dedicat\xf3ria",
                        es: "Nombre de la dedicatoria"
                    },
                    customize_dedication_person_name_hint: {
                        pt: "Nome dessa pessoa",
                        es: "Nombre de la persona"
                    },
                    customize_dedication_person_email_label: {
                        pt: "E-mail da dedicat\xf3ria",
                        es: "Email de la dedicatoria"
                    },
                    customize_dedication_person_email_hint: {
                        pt: "Endere\xe7o de e-mail para notifica\xe7\xe3o",
                        es: "Correo electr\xf3nico para notificar"
                    },
                    customize_dedication_person_message_label: {
                        pt: "Mensagem da dedicat\xf3ria",
                        es: "Mensaje de dedicatoria"
                    },
                    customize_dedication_person_message_hint: {
                        pt: "Adicione uma mensagem a esta dedicat\xf3ria",
                        es: "A\xf1ade un mensaje a esta dedicaci\xf3n"
                    },
                    customize_dedication_hover: {
                        pt: 'Voc\xea pode dedicar sua doa\xe7\xe3o a algu\xe9m especial. Se voc\xea gostaria de notificar a pessoa ou algu\xe9m mais sobre sua dedicat\xf3ria, adicione o e-mail dela no campo que diz "Endere\xe7o de e-mail para notifica\xe7\xe3o". Sua mensagem e informa\xe7\xf5es sobre sua doa\xe7\xe3o ser\xe3o enviadas para este endere\xe7o de e-mail.',
                        es: 'Puedes dedicar tu obsequio a alguien especial. Si quieres notificar a la persona o a alguien m\xe1s, a\xf1ade su correo electr\xf3nico en el campo titulado "Correo electr\xf3nico para notificar". Tu mensaje e informaci\xf3n sobre tu obsequio ser\xe1 enviado a ese correo electr\xf3nico.'
                    },
                    customize_dedication_name: {
                        pt: "Dedique esta doa\xe7\xe3o",
                        es: "Dedica este donativo"
                    },
                    customize_company_match_company_name: {
                        pt: "Nome da empresa",
                        es: "Nombre de la empresa"
                    },
                    customize_company_match_company_email: {
                        pt: "E-mail da empresa",
                        es: "Correo electr\xf3nico de la empresa"
                    },
                    customize_company_match_company_email_placeholder: {
                        pt: "exemplo@exemplo.com",
                        es: "ejemplo@ejemplo.com"
                    },
                    customize_company_match_search_companies: {
                        pt: "Buscar empresas",
                        es: "Buscar empresas"
                    },
                    customize_company_match_name: {
                        pt: "Minha empresa tem um programa de doa\xe7\xf5es",
                        es: "Mi empresa cuenta con un programa de donativos"
                    },
                    customize_company_match_min_match_error: {
                        pt: "O valor \xe9 menor do que o valor em d\xf3lares especificado para ser dobrado pela empresa",
                        es: "El monto es menor que el monto en d\xf3lares especificado para duplicar por la empresa"
                    },
                    customize_company_match_hint: {
                        pt: "Muitas empresas dobram os presentes dados por seus funcion\xe1rios. Selecione esta op\xe7\xe3o para ver se sua empresa possui um programa de doa\xe7\xf5es.",
                        es: "Muchas empresas duplican los obsequios otorgados por sus empleados. Selecciona esta opci\xf3n para ver si tu empresa cuenta con un programa de donativos."
                    },
                    customize_comment_name: {
                        pt: "Comentar",
                        es: "Comentar"
                    },
                    customize_comment_place_holder: {
                        pt: "Deixe um coment\xe1rio",
                        es: "Dejar un comentario"
                    },
                    customize_anonymous_name: {
                        pt: "Fazer uma doa\xe7\xe3o an\xf4nima",
                        es: "Aportar an\xf3nimamente"
                    },
                    customize_anonymous_hover: {
                        pt: "Quando voc\xea doa anonimamente, seu nome nunca aparecer\xe1 em p\xfablico como um doador. Entretanto, seu nome ser\xe1 salvo para que possamos enviar o recibo da sua doa\xe7\xe3o.",
                        es: "Cuando aportas an\xf3nimamente, tu nombre nunca aparece en p\xfablico como aliado. Pero guardaremos tu nombre para poder mandarte el recibo."
                    },
                    customize_accept_button_label: {
                        es: "Hecho",
                        pt: "Feito"
                    },
                    form_header_text: {
                        en: "Make a Donation",
                        es: "Haz una donaci\xf3n"
                    },
                    general_next_button: {
                        pt: "Pr\xf3ximo",
                        es: "Siguiente"
                    },
                    general_next_button_aria: {
                        pt: "Continuar",
                        es: "Continuar"
                    },
                    general_ssl: {
                        pt: "As informa\xe7\xf5es enviadas por meio deste formul\xe1rio s\xe3o criptografadas e transmitidas por meio de uma conex\xe3o SSL segura",
                        es: "La informaci\xf3n enviada a trav\xe9s de este formulario es cifrada y transmitida por una conexi\xf3n SSL segura"
                    },
                    general_recaptcha_invalid: {
                        pt: "Falha ao resolver o reCAPTCHA",
                        es: "Falla al ingresar el reCAPTCHA"
                    },
                    general_email_invalid: {
                        pt: "Voc\xea deve inserir um e-mail v\xe1lido",
                        es: "Debes agregar un correo electr\xf3nico v\xe1lido"
                    },
                    general_dates_month_label: {
                        pt: "M\xeas",
                        es: "Mes"
                    },
                    general_dates_month_invalid: {
                        pt: "M\xeas inv\xe1lido",
                        es: "Mes inv\xe1lido"
                    },
                    general_dates_day_label: {
                        pt: "Dia",
                        es: "D\xeda"
                    },
                    general_dates_day_invalid: {
                        pt: "Dia inv\xe1lido",
                        es: "D\xeda inv\xe1lido"
                    },
                    general_dates_year_label: {
                        pt: "Ano",
                        es: "A\xf1o"
                    },
                    general_dates_year_invalid: {
                        pt: "Ano inv\xe1lido",
                        es: "A\xf1o inv\xe1lido"
                    },
                    general_field_is_required: {
                        pt: "Este campo \xe9 obrigat\xf3rio",
                        es: "Este campo es obligatorio"
                    },
                    general_group_complete_fields: {
                        pt: "Favor preencher os campos obrigat\xf3rios",
                        es: "Completa los campos obligatorios"
                    },
                    general_field_must_be_number: {
                        pt: "Este campo deve conter apenas n\xfameros",
                        es: "Este campo s\xf3lo debe contener n\xfameros"
                    },
                    general_donate_securely: {
                        en: "Donate Securely",
                        es: "Donar de forma segura"
                    },
                    payments_currency: {
                        pt: "Moeda",
                        es: "Moneda"
                    },
                    payments_methods_name: {
                        pt: "M\xe9todo de pagamento",
                        es: "Forma de pago"
                    },
                    payments_methods_apple_pay: {
                        pt: "Apple Pay",
                        es: "Apple Pay"
                    },
                    payments_methods_card: {
                        pt: "Cart\xe3o de d\xe9bito/cr\xe9dito",
                        es: "Tarjeta de d\xe9bito/cr\xe9dito"
                    },
                    payments_methods_paypal: {
                        pt: "PayPal",
                        es: "PayPal"
                    },
                    payments_methods_check: {
                        pt: "Cheque eletr\xf4nico",
                        es: "Cheque electr\xf3nico"
                    },
                    payments_methods_bitcoin: {
                        pt: "Bitcoin",
                        es: "Bitcoin"
                    },
                    payments_check_savings: {
                        pt: "D\xe9bito em conta",
                        es: "Cuenta de ahorro"
                    },
                    payments_check_checking: {
                        pt: "Conta de cheques",
                        es: "Cuenta de cheques"
                    },
                    payments_check_personal: {
                        pt: "Pessoal",
                        es: "Personal"
                    },
                    payments_check_business: {
                        pt: "Corporativo",
                        es: "Negocio"
                    },
                    payments_check_bank_account_type: {
                        pt: "Tipo de Conta",
                        es: "Tipo de Cuenta"
                    },
                    payments_check_bank_account_holder_type: {
                        pt: "Titular da Conta",
                        es: "Titular de la Cuenta"
                    },
                    payments_check_bank_name: {
                        pt: "Nome do Banco",
                        es: "Nombre del Banco"
                    },
                    payments_check_bank_account_number: {
                        pt: "N\xfamero da conta banc\xe1ria",
                        es: "N\xfamero de cuenta bancaria"
                    },
                    payments_check_bank_routing_number: {
                        pt: "N\xfamero de identifica\xe7\xe3o do Banco",
                        es: "N\xfamero de identificaci\xf3n del Banco"
                    },
                    payments_credit_card_cvv_hint: {
                        pt: "O c\xf3digo CVV est\xe1 no verso do cart\xe3o. American Express CVV est\xe1 na frente.",
                        es: "El CVV (o c\xf3digo de verificaci\xf3n) est\xe1 com\xfanmente al reverso de las tarjetas. El CVV de American Express est\xe1 al frente."
                    },
                    payments_credit_card_cvv_label: {
                        pt: "CVV do cart\xe3o",
                        es: "CVV de la tarjeta"
                    },
                    payments_credit_card_number_label: {
                        pt: "N\xfamero do cart\xe3o",
                        es: "N\xfamero de tarjeta"
                    },
                    payments_credit_card_number_error: {
                        pt: "O n\xfamero do cart\xe3o \xe9 inv\xe1lido",
                        es: "El n\xfamero de tarjeta es inv\xe1lido"
                    },
                    payments_credit_card_cvv_error: {
                        pt: "O CVV \xe9 inv\xe1lido",
                        es: "El CVV es inv\xe1lido"
                    },
                    payments_credit_card_number_placeholder: {
                        pt: "N\xfamero do cart\xe3o",
                        es: "N\xfamero de tarjeta"
                    },
                    payments_cvv_placeholder: {
                        pt: "cvv",
                        es: "cvv"
                    },
                    payments_credit_card_submit: {
                        pt: "Pagar",
                        es: "Pagar"
                    },
                    payments_credit_card_progress: {
                        pt: "Processando o pagamento",
                        es: "Procesando el pago"
                    },
                    payments_check_submit: {
                        pt: "Pagamento completo",
                        es: "Pago completado"
                    },
                    payments_check_progress: {
                        pt: "Processando o pagamento",
                        es: "Procesando el pago"
                    },
                    payments_bitcoin_submit: {
                        pt: "Finalizar com BitPay",
                        es: "Terminar con BitPay"
                    },
                    payments_bitcoin_progress: {
                        pt: "Comunicando com BitPay",
                        es: "Comunicando con BitPay"
                    },
                    payments_paypal_submit: {
                        pt: "Finalizar com PayPal",
                        es: "Terminar con PayPal"
                    },
                    payments_paypal_progress: {
                        pt: "Comunicando com PayPal",
                        es: "Comunicando con PayPal"
                    },
                    payments_paypal_success: {
                        pt: "Por favor, termine a sua transa\xe7\xe3o com o PayPal.",
                        es: "Por favor termina tu transacci\xf3n con PayPal."
                    },
                    payments_bitcoin_success: {
                        pt: "Por favor, termine a sua transa\xe7\xe3o com BitPay.",
                        es: "Por favor termina tu transacci\xf3n con BitPay."
                    },
                    payments_apple_pay_progress: {
                        pt: "Processando sua transa\xe7\xe3o com Apple Pay",
                        es: "Procesando tu transacci\xf3n con Apple Pay"
                    },
                    payments_failed_try_again: {
                        pt: "Tentar novamente",
                        es: "Intentar de nuevo"
                    },
                    payments_thank_you_supporter: {
                        pt: "Obrigado, %{name}!",
                        es: "Gracias, %{name}"
                    },
                    payments_thank_you_receipt: {
                        pt: "Voc\xea receber\xe1 seu recibo por e-mail em breve.",
                        es: "Recibir\xe1s un recibo por correo electr\xf3nico en un momento."
                    },
                    order_your: {
                        pt: "Seu pedido.",
                        es: "Tu orden"
                    },
                    order_items_heading: {
                        pt: "Selecione o n\xfamero de ingressos",
                        es: "Selecciona el n\xfamero de boletos"
                    },
                    order_items_submit: {
                        pt: "Fazer pedido",
                        es: "Ordenar"
                    },
                    order_items_errors_empty: {
                        pt: "\xc9 necess\xe1rio ao menos um ingresso",
                        es: "Es necesario al menos un boleto"
                    },
                    order_registrations_heading: {
                        pt: "Personalize seu ingresso %{position} de %{total}",
                        es: "Personaliza tu boleto %{position} de %{total}"
                    },
                    order_registrations_for_me: {
                        pt: "Este ingresso \xe9 para mim",
                        es: "Este boleto es para mi"
                    },
                    order_registrations_guest_heading: {
                        pt: "Informa\xe7\xf5es do convidado",
                        es: "Informaci\xf3n del invitado"
                    },
                    validation_default: {
                        pt: "Algo est\xe1 errado. Por favor, verifique as informa\xe7\xf5es inseridas.",
                        es: "Algo es incorrecto, por favor revisa la informaci\xf3n ingresada."
                    },
                    validation_missing: {
                        pt: "Falta %{label}",
                        es: "Falta %{label}"
                    },
                    validation_required: {
                        pt: "%{label} \xe9 necess\xe1rio",
                        es: "%{label} es necesario"
                    },
                    institution_checkbox_label: {
                        pt: "Essa doa\xe7\xe3o \xe9 em nome de uma institui\xe7\xe3o",
                        es: "Este obsequio es en nombre de una instituci\xf3n"
                    },
                    institution_name: {
                        pt: "Nome da institui\xe7\xe3o",
                        es: "Nombre de la instituci\xf3n"
                    },
                    institution_type: {
                        pt: "Tipo de institui\xe7\xe3o",
                        es: "Tipo de instituci\xf3n"
                    },
                    institution_name_place_holder: {
                        pt: "insira o nome da institui\xe7\xe3o",
                        es: "ingresar el nombre de la instituci\xf3n"
                    },
                    institution_categories_corporation: {
                        pt: "Corpora\xe7\xe3o",
                        es: "Corporaci\xf3n"
                    },
                    institution_categories_foundation: {
                        pt: "Funda\xe7\xe3o",
                        es: "Fundaci\xf3n"
                    },
                    institution_categories_place_of_worship: {
                        pt: "Local de culto",
                        es: "Lugar de culto"
                    },
                    institution_categories_government: {
                        pt: "Governo",
                        es: "Gobierno"
                    },
                    institution_categories_school: {
                        pt: "Escola",
                        es: "Escuela"
                    },
                    institution_categories_donor_advised_fund: {
                        pt: "Fundos Aconselhados por Doadores",
                        es: "Fondos asesorados por Donantes"
                    },
                    institution_categories_other: {
                        pt: "Outro",
                        es: "Otro"
                    },
                    donor_information_first_name: {
                        pt: "Nome",
                        es: "Nombre"
                    },
                    donor_information_last_name: {
                        pt: "Sobrenome",
                        es: "Apellido"
                    },
                    donor_information_email: {
                        pt: "E-mail",
                        es: "Correo electr\xf3nico"
                    },
                    donor_information_email_opt_in: {
                        pt: "Quero receber atualiza\xe7\xf5es por e-mail",
                        es: "Mantenerme informado sobre mi aportaci\xf3n por correo electr\xf3nico."
                    },
                    donor_information_phone: {
                        pt: "Telefone",
                        es: "Tel\xe9fono"
                    },
                    donor_information_address: {
                        pt: "Endere\xe7o",
                        es: "Direcci\xf3n"
                    },
                    donor_information_birthday_month: {
                        pt: "M\xeas",
                        es: "Mes"
                    },
                    donor_information_birthday_day: {
                        pt: "Dia",
                        es: "D\xeda"
                    },
                    donor_information_birthday_year: {
                        pt: "Ano",
                        es: "A\xf1o"
                    },
                    donor_information_gender_women: {
                        pt: "Mulher",
                        es: "Mujer"
                    },
                    donor_information_gender_man: {
                        pt: "Homem",
                        es: "Hombre"
                    },
                    donor_information_gender_prefer_not_to_say: {
                        pt: "Prefiro n\xe3o dizer",
                        es: "Prefiero no decir"
                    },
                    donor_information_gender_non_binary_other: {
                        pt: "N\xe3o-bin\xe1rio/Outro",
                        es: "No-Binario/Otro"
                    },
                    donor_information_auto_address_manual_entry: {
                        pt: "Entrada Manual",
                        es: "Entrada Manual"
                    },
                    donor_information_auto_address_server_error: {
                        pt: "Ocorreu um erro ao recuperar os detalhes do endere\xe7o. Por favor, insira o endere\xe7o manualmente.",
                        es: "Hubo un error recuperando los datos de la direcci\xf3n. Por favor ingresar la direcci\xf3n manualmente."
                    },
                    donor_information_auto_address_label: {
                        pt: "Buscar endere\xe7o",
                        es: "Buscar la Direcci\xf3n"
                    },
                    donor_information_auto_address_place_holder: {
                        pt: "Insira o Endere\xe7o",
                        es: "Ingresa la Direcci\xf3n"
                    },
                    donor_information_city: {
                        pt: "Cidade",
                        es: "Ciudad"
                    },
                    donor_information_state: {
                        pt: "Estado",
                        es: "Estado"
                    },
                    donor_information_postal_code: {
                        pt: "CEP",
                        es: "C\xf3digo postal"
                    },
                    donor_information_postal_code_error: {
                        pt: "Erro no c\xf3digo postal",
                        es: "Error en el c\xf3digo postal"
                    },
                    donor_information_country: {
                        pt: "Pa\xeds",
                        es: "Pa\xeds"
                    },
                    donor_information_or_enter_postal_code: {
                        pt: "insira o c\xf3digo postal",
                        es: "o ingresa el c\xf3digo postal"
                    },
                    donor_information_heading: {
                        pt: "Suas informa\xe7\xf5es",
                        es: "Tu informaci\xf3n"
                    },
                    donor_information_billing: {
                        pt: "Endere\xe7o de cobran\xe7a",
                        es: "Domicilio de facturaci\xf3n"
                    },
                    donation_amount_text: {
                        pt: "Valor da doa\xe7\xe3o",
                        es: "Monto del obsequio"
                    },
                    general_done_button: {
                        pt: "Feito",
                        es: "Hecho"
                    },
                    general_edit_button: {
                        pt: "Editar",
                        es: "Editar"
                    },
                    payments_total_payment_amount: {
                        pt: "Valor total do pagamento",
                        es: "Monto total"
                    }
                }
            }
        };
    }
    static funraiseAware(f, u, n, r, a, scriptOnLoad) {
        const data = {
            window: window,
            document: document,
            tag: "script",
            data: "funraise",
            orgId: f,
            uri: u,
            common: n,
            client: r,
            script: a
        };
        data.window[data.data] = data.window[data.data] || [];
        if (data.window[data.data].scriptIsLoading || data.window[data.data].scriptIsLoaded) return;
        data.window[data.data].loading = true;
        data.window[data.data].push("init", data);
        const scripts = data.document.getElementsByTagName(data.tag)[0];
        const funraiseScript = data.document.createElement(data.tag);
        funraiseScript.async = true;
        funraiseScript.onload = scriptOnLoad;
        funraiseScript.src = data.uri + data.common + data.script + "?orgId=" + data.orgId;
        scripts.parentNode.insertBefore(funraiseScript, scripts);
    }
    static success(donation) {
        if (Array.isArray(window.dataLayer)) window.dataLayer.push({
            event: "donation:funraise",
            funraiseDonationData: donation
        });
    }
    static initForms(formIds) {
        formIds.forEach((formId)=>{
            window.funraise.push("create", {
                form: formId
            });
            if (window.funraise && window.funraise.push) {
                window.funraise.push("onSuccess", {
                    form: formId
                }, (donor, donation)=>{
                    $d582dd6eaccab18d$var$AEFunraise.success(donation);
                });
                window.funraise.push("config", {
                    form: formId
                }, $d582dd6eaccab18d$var$AEFunraise.getSettings());
            }
        });
    }
}
var $d582dd6eaccab18d$export$2e2bcd8739ae039 = (selector)=>{
    const formButtons = document.querySelectorAll(selector);
    const formsToInitialize = [];
    formButtons.forEach((formButton)=>{
        const formId = formButton.getAttribute("data-formId");
        if (formId && !isNaN(formId) && formsToInitialize.findIndex((form)=>form === formId) === -1) formsToInitialize.push(formId);
    });
    if (!window.funraiseOrgId || !formsToInitialize.length) return;
    $d582dd6eaccab18d$var$AEFunraise.funraiseAware(window.funraiseOrgId, "https://assets.funraise.io", "/widget/common/2.0", "/widget/client", "/inject-form.js", ()=>$d582dd6eaccab18d$var$AEFunraise.initForms(formsToInitialize));
};


export {$d582dd6eaccab18d$export$2e2bcd8739ae039 as default};
