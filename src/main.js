class AEFunraise {
  static getSettings() {
    return {
      i18n: {
        locale: window.navigator.language,
        phrases: {
          donation_give: {
            pt: 'Uma',
            es: 'Donar'
          },
          donation_frequency_once: {
            pt: 'uma vez',
            es: 'una vez'
          },
          donation_give_once: {
            pt: 'Uma vez',
            es: 'Una vez'
          },
          donation_ask_amount: {
            pt: 'Escolha um valor para doar',
            es: 'Elige la cantidad a donar'
          },
          donation_donate_with: {
            pt: 'Doar com',
            es: 'Contribuir con'
          },
          payments_cover_transaction_fees: {
            pt: 'Cobrir as tarifas bancárias',
            es: 'Cubrir las tarifas bancarias'
          },
          payments_cover_fees_message: {
            pt: 'Quando você aumenta sua doação para cobrir as taxas de transação, está ajudando a %{org_name} a utilizar mais de sua doação.',
            es: 'Cuando aumentas tu contribución para cubrir las tarifas de transacción, estás ayudando a %{org_name} a poner más de tu aportación en uso.'
          },
          payments_cover_fees_label: {
            pt: 'Sim, cobrir a tarifa',
            es: 'Sí, cubrir la tarifa'
          },
          donor_information_address_line_2: {
            pt: 'Linha de endereço 2',
            es: 'Línea de dirección 2'
          },
          payments_credit_card_information_title: {
            pt: 'Informações do cartão',
            es: 'Información de la tarjeta'
          },
          payments_pay_total: {
            pt: 'Pagar',
            es: 'Pagar'
          },
          payments_thank_you_message_1: {
            pt: 'Seu pagamento foi processado com sucesso.',
            es: 'Tu pago se ha procesado correctamente.'
          },
          payments_thank_you_message_2: {
            pt: 'Você receberá um recibo por e-mail em breve',
            es: 'En breve recibirás un recibo por correo electrónico'
          },
          payments_credit_card_expired_error: {
            pt: 'Seu cartão expirou',
            es: 'Tu tarjeta está vencida'
          },
          donation_optional_donation: {
            pt: 'Adicionar uma doação voluntária',
            es: 'Añadir una aportación voluntaria'
          },
          donation_select_amount: {
            pt: 'Selecione o valor a ser doado',
            es: 'Selecciona el monto del obsequio en pesos mexicanos'
          },
          donation_or_enter_amount: {
            pt: 'ou escreva um valor',
            es: 'o ingresa el monto en pesos mexicanos'
          },
          donation_allocation_donate_to: {
            pt: 'Doar para',
            es: 'Donar a'
          },
          donation_frequency_name: {
            pt: 'Frequência',
            es: 'Frecuencia'
          },
          donation_frequency_one_time: {
            pt: 'Uma única vez',
            es: 'Por única vez'
          },
          donation_frequency_weekly: {
            pt: 'Semanal',
            es: 'Semanal'
          },
          donation_frequency_monthly: {
            pt: 'Mensalmente',
            es: 'Mensualmente'
          },
          donation_frequency_quarterly: {
            pt: 'Trimestral',
            es: 'Trimestral'
          },
          donation_frequency_yearly: {
            pt: 'Anual',
            es: 'Anual'
          },
          donation_ask_amount_once: {
            pt: 'Contribua uma vez',
            es: 'Contribuye una vez'
          },
          donation_ask_amount_monthly: {
            pt: 'Contribua mensalmente',
            es: 'Contribuye mensualmente'
          },
          donation_your: {
            pt: 'Sua doação',
            es: 'Tu obsequio'
          },
          donation_amount: {
            pt: 'Quantia',
            es: 'Monto'
          },
          donation_remove_amount: {
            pt: 'Retirar',
            es: 'Quitar'
          },
          donation_options: {
            es: 'Opciones de donación',
            pt: 'Opções de donativos'
          },
          donation_min_validation: {
            pt: 'O valor deve ser pelo menos de %{currency_symbol}%{amount}',
            es: 'La cantidad debe ser al menos %{currency_symbol}%{amount} pesos'
          },
          donation_max_validation: {
            pt: 'O valor deve ser de no máximo %{currency_symbol}%{amount}',
            es: 'Cantidad puede ser máximo %{currency_symbol}%{amount} pesos'
          },
          customize_question_this: {
            pt: 'Esta pergunta',
            es: 'Esta pregunta'
          },
          customize_question_select_an_option: {
            pt: 'Selecione uma opção',
            es: 'Selecciona una opción'
          },
          customize_dedication_in_honor_label: {
            pt: 'Em honra de',
            es: 'En honor a'
          },
          customize_dedication_in_memory_label: {
            pt: 'Em memória de',
            es: 'En memoria de'
          },
          customize_dedication_inspired_by_label: {
            pt: 'Inspirado por',
            es: 'Inspirado por'
          },
          customize_dedication_type_hint: {
            pt: 'Selecione um tipo de dedicatória',
            es: 'Seleccione un tipo de dedicatoria'
          },
          customize_dedication_type_label: {
            pt: 'Tipo de dedicatória',
            es: 'Tipo de dedicatoria'
          },
          customize_dedication_person_name_label: {
            pt: 'Nome da dedicatória',
            es: 'Nombre de la dedicatoria'
          },
          customize_dedication_person_name_hint: {
            pt: 'Nome dessa pessoa',
            es: 'Nombre de la persona'
          },
          customize_dedication_person_email_label: {
            pt: 'E-mail da dedicatória',
            es: 'Email de la dedicatoria'
          },
          customize_dedication_person_email_hint: {
            pt: 'Endereço de e-mail para notificação',
            es: 'Correo electrónico para notificar'
          },
          customize_dedication_person_message_label: {
            pt: 'Mensagem da dedicatória',
            es: 'Mensaje de dedicatoria'
          },
          customize_dedication_person_message_hint: {
            pt: 'Adicione uma mensagem a esta dedicatória',
            es: 'Añade un mensaje a esta dedicación'
          },
          customize_dedication_hover: {
            pt: 'Você pode dedicar sua doação a alguém especial. Se você gostaria de notificar a pessoa ou alguém mais sobre sua dedicatória, adicione o e-mail dela no campo que diz "Endereço de e-mail para notificação". Sua mensagem e informações sobre sua doação serão enviadas para este endereço de e-mail.',
            es: 'Puedes dedicar tu obsequio a alguien especial. Si quieres notificar a la persona o a alguien más, añade su correo electrónico en el campo titulado "Correo electrónico para notificar". Tu mensaje e información sobre tu obsequio será enviado a ese correo electrónico.'
          },
          customize_dedication_name: {
            pt: 'Dedique esta doação',
            es: 'Dedica este donativo'
          },
          customize_company_match_company_name: {
            pt: 'Nome da empresa',
            es: 'Nombre de la empresa'
          },
          customize_company_match_company_email: {
            pt: 'E-mail da empresa',
            es: 'Correo electrónico de la empresa'
          },
          customize_company_match_company_email_placeholder: {
            pt: 'exemplo@exemplo.com',
            es: 'ejemplo@ejemplo.com'
          },
          customize_company_match_search_companies: {
            pt: 'Buscar empresas',
            es: 'Buscar empresas'
          },
          customize_company_match_name: {
            pt: 'Minha empresa tem um programa de doações',
            es: 'Mi empresa cuenta con un programa de donativos'
          },
          customize_company_match_min_match_error: {
            pt: 'O valor é menor do que o valor em dólares especificado para ser dobrado pela empresa',
            es: 'El monto es menor que el monto en dólares especificado para duplicar por la empresa'
          },
          customize_company_match_hint: {
            pt: 'Muitas empresas dobram os presentes dados por seus funcionários. Selecione esta opção para ver se sua empresa possui um programa de doações.',
            es: 'Muchas empresas duplican los obsequios otorgados por sus empleados. Selecciona esta opción para ver si tu empresa cuenta con un programa de donativos.'
          },
          customize_comment_name: {
            pt: 'Comentar',
            es: 'Comentar'
          },
          customize_comment_place_holder: {
            pt: 'Deixe um comentário',
            es: 'Dejar un comentario'
          },
          customize_anonymous_name: {
            pt: 'Fazer uma doação anônima',
            es: 'Aportar anónimamente'
          },
          customize_anonymous_hover: {
            pt: 'Quando você doa anonimamente, seu nome nunca aparecerá em público como um doador. Entretanto, seu nome será salvo para que possamos enviar o recibo da sua doação.',
            es: 'Cuando aportas anónimamente, tu nombre nunca aparece en público como aliado. Pero guardaremos tu nombre para poder mandarte el recibo.'
          },
          customize_accept_button_label: {
            es: 'Hecho',
            pt: 'Feito'
          },
          form_header_text: {
            en: 'Make a Donation',
            es: 'Haz una donación'
          },
          general_next_button: {
            pt: 'Próximo',
            es: 'Siguiente'
          },
          general_next_button_aria: {
            pt: 'Continuar',
            es: 'Continuar'
          },
          general_ssl: {
            pt: 'As informações enviadas por meio deste formulário são criptografadas e transmitidas por meio de uma conexão SSL segura',
            es: 'La información enviada a través de este formulario es cifrada y transmitida por una conexión SSL segura'
          },
          general_recaptcha_invalid: {
            pt: 'Falha ao resolver o reCAPTCHA',
            es: 'Falla al ingresar el reCAPTCHA'
          },
          general_email_invalid: {
            pt: 'Você deve inserir um e-mail válido',
            es: 'Debes agregar un correo electrónico válido'
          },
          general_dates_month_label: {
            pt: 'Mês',
            es: 'Mes'
          },
          general_dates_month_invalid: {
            pt: 'Mês inválido',
            es: 'Mes inválido'
          },
          general_dates_day_label: {
            pt: 'Dia',
            es: 'Día'
          },
          general_dates_day_invalid: {
            pt: 'Dia inválido',
            es: 'Día inválido'
          },
          general_dates_year_label: {
            pt: 'Ano',
            es: 'Año'
          },
          general_dates_year_invalid: {
            pt: 'Ano inválido',
            es: 'Año inválido'
          },
          general_field_is_required: {
            pt: 'Este campo é obrigatório',
            es: 'Este campo es obligatorio'
          },
          general_group_complete_fields: {
            pt: 'Favor preencher os campos obrigatórios',
            es: 'Completa los campos obligatorios'
          },
          general_field_must_be_number: {
            pt: 'Este campo deve conter apenas números',
            es: 'Este campo sólo debe contener números'
          },
          general_donate_securely: {
            en: 'Donate Securely',
            es: 'Donar de forma segura'
          },
          payments_currency: {
            pt: 'Moeda',
            es: 'Moneda'
          },
          payments_methods_name: {
            pt: 'Método de pagamento',
            es: 'Forma de pago'
          },
          payments_methods_apple_pay: {
            pt: 'Apple Pay',
            es: 'Apple Pay'
          },
          payments_methods_card: {
            pt: 'Cartão de débito/crédito',
            es: 'Tarjeta de débito/crédito'
          },
          payments_methods_paypal: {
            pt: 'PayPal',
            es: 'PayPal'
          },
          payments_methods_check: {
            pt: 'Cheque eletrônico',
            es: 'Cheque electrónico'
          },
          payments_methods_bitcoin: {
            pt: 'Bitcoin',
            es: 'Bitcoin'
          },
          payments_check_savings: {
            pt: 'Débito em conta',
            es: 'Cuenta de ahorro'
          },
          payments_check_checking: {
            pt: 'Conta de cheques',
            es: 'Cuenta de cheques'
          },
          payments_check_personal: {
            pt: 'Pessoal',
            es: 'Personal'
          },
          payments_check_business: {
            pt: 'Corporativo',
            es: 'Negocio'
          },
          payments_check_bank_account_type: {
            pt: 'Tipo de Conta',
            es: 'Tipo de Cuenta'
          },
          payments_check_bank_account_holder_type: {
            pt: 'Titular da Conta',
            es: 'Titular de la Cuenta'
          },
          payments_check_bank_name: {
            pt: 'Nome do Banco',
            es: 'Nombre del Banco'
          },
          payments_check_bank_account_number: {
            pt: 'Número da conta bancária',
            es: 'Número de cuenta bancaria'
          },
          payments_check_bank_routing_number: {
            pt: 'Número de identificação do Banco',
            es: 'Número de identificación del Banco'
          },
          payments_credit_card_cvv_hint: {
            pt: 'O código CVV está no verso do cartão. American Express CVV está na frente.',
            es: 'El CVV (o código de verificación) está comúnmente al reverso de las tarjetas. El CVV de American Express está al frente.'
          },
          payments_credit_card_cvv_label: {
            pt: 'CVV do cartão',
            es: 'CVV de la tarjeta'
          },
          payments_credit_card_number_label: {
            pt: 'Número do cartão',
            es: 'Número de tarjeta'
          },
          payments_credit_card_number_error: {
            pt: 'O número do cartão é inválido',
            es: 'El número de tarjeta es inválido'
          },
          payments_credit_card_cvv_error: {
            pt: 'O CVV é inválido',
            es: 'El CVV es inválido'
          },
          payments_credit_card_number_placeholder: {
            pt: 'Número do cartão',
            es: 'Número de tarjeta'
          },
          payments_cvv_placeholder: {
            pt: 'cvv',
            es: 'cvv'
          },
          payments_credit_card_submit: {
            pt: 'Pagar',
            es: 'Pagar'
          },
          payments_credit_card_progress: {
            pt: 'Processando o pagamento',
            es: 'Procesando el pago'
          },
          payments_check_submit: {
            pt: 'Pagamento completo',
            es: 'Pago completado'
          },
          payments_check_progress: {
            pt: 'Processando o pagamento',
            es: 'Procesando el pago'
          },
          payments_bitcoin_submit: {
            pt: 'Finalizar com BitPay',
            es: 'Terminar con BitPay'
          },
          payments_bitcoin_progress: {
            pt: 'Comunicando com BitPay',
            es: 'Comunicando con BitPay'
          },
          payments_paypal_submit: {
            pt: 'Finalizar com PayPal',
            es: 'Terminar con PayPal'
          },
          payments_paypal_progress: {
            pt: 'Comunicando com PayPal',
            es: 'Comunicando con PayPal'
          },
          payments_paypal_success: {
            pt: 'Por favor, termine a sua transação com o PayPal.',
            es: 'Por favor termina tu transacción con PayPal.'
          },
          payments_bitcoin_success: {
            pt: 'Por favor, termine a sua transação com BitPay.',
            es: 'Por favor termina tu transacción con BitPay.'
          },
          payments_apple_pay_progress: {
            pt: 'Processando sua transação com Apple Pay',
            es: 'Procesando tu transacción con Apple Pay'
          },
          payments_failed_try_again: {
            pt: 'Tentar novamente',
            es: 'Intentar de nuevo'
          },
          payments_thank_you_supporter: {
            pt: 'Obrigado, %{name}!',
            es: 'Gracias, %{name}'
          },
          payments_thank_you_receipt: {
            pt: 'Você receberá seu recibo por e-mail em breve.',
            es: 'Recibirás un recibo por correo electrónico en un momento.'
          },
          order_your: {
            pt: 'Seu pedido.',
            es: 'Tu orden'
          },
          order_items_heading: {
            pt: 'Selecione o número de ingressos',
            es: 'Selecciona el número de boletos'
          },
          order_items_submit: {
            pt: 'Fazer pedido',
            es: 'Ordenar'
          },
          order_items_errors_empty: {
            pt: 'É necessário ao menos um ingresso',
            es: 'Es necesario al menos un boleto'
          },
          order_registrations_heading: {
            pt: 'Personalize seu ingresso %{position} de %{total}',
            es: 'Personaliza tu boleto %{position} de %{total}'
          },
          order_registrations_for_me: {
            pt: 'Este ingresso é para mim',
            es: 'Este boleto es para mi'
          },
          order_registrations_guest_heading: {
            pt: 'Informações do convidado',
            es: 'Información del invitado'
          },
          validation_default: {
            pt: 'Algo está errado. Por favor, verifique as informações inseridas.',
            es: 'Algo es incorrecto, por favor revisa la información ingresada.'
          },
          validation_missing: {
            pt: 'Falta %{label}',
            es: 'Falta %{label}'
          },
          validation_required: {
            pt: '%{label} é necessário',
            es: '%{label} es necesario'
          },
          institution_checkbox_label: {
            pt: 'Essa doação é em nome de uma instituição',
            es: 'Este obsequio es en nombre de una institución'
          },
          institution_name: {
            pt: 'Nome da instituição',
            es: 'Nombre de la institución'
          },
          institution_type: {
            pt: 'Tipo de instituição',
            es: 'Tipo de institución'
          },
          institution_name_place_holder: {
            pt: 'insira o nome da instituição',
            es: 'ingresar el nombre de la institución'
          },
          institution_categories_corporation: {
            pt: 'Corporação',
            es: 'Corporación'
          },
          institution_categories_foundation: {
            pt: 'Fundação',
            es: 'Fundación'
          },
          institution_categories_place_of_worship: {
            pt: 'Local de culto',
            es: 'Lugar de culto'
          },
          institution_categories_government: {
            pt: 'Governo',
            es: 'Gobierno'
          },
          institution_categories_school: {
            pt: 'Escola',
            es: 'Escuela'
          },
          institution_categories_donor_advised_fund: {
            pt: 'Fundos Aconselhados por Doadores',
            es: 'Fondos asesorados por Donantes'
          },
          institution_categories_other: {
            pt: 'Outro',
            es: 'Otro'
          },
          donor_information_first_name: {
            pt: 'Nome',
            es: 'Nombre'
          },
          donor_information_last_name: {
            pt: 'Sobrenome',
            es: 'Apellido'
          },
          donor_information_email: {
            pt: 'E-mail',
            es: 'Correo electrónico'
          },
          donor_information_email_opt_in: {
            pt: 'Quero receber atualizações por e-mail',
            es: 'Mantenerme informado sobre mi aportación por correo electrónico.'
          },
          donor_information_phone: {
            pt: 'Telefone',
            es: 'Teléfono'
          },
          donor_information_address: {
            pt: 'Endereço',
            es: 'Dirección'
          },
          donor_information_birthday_month: {
            pt: 'Mês',
            es: 'Mes'
          },
          donor_information_birthday_day: {
            pt: 'Dia',
            es: 'Día'
          },
          donor_information_birthday_year: {
            pt: 'Ano',
            es: 'Año'
          },
          donor_information_gender_women: {
            pt: 'Mulher',
            es: 'Mujer'
          },
          donor_information_gender_man: {
            pt: 'Homem',
            es: 'Hombre'
          },
          donor_information_gender_prefer_not_to_say: {
            pt: 'Prefiro não dizer',
            es: 'Prefiero no decir'
          },
          donor_information_gender_non_binary_other: {
            pt: 'Não-binário/Outro',
            es: 'No-Binario/Otro'
          },
          donor_information_auto_address_manual_entry: {
            pt: 'Entrada Manual',
            es: 'Entrada Manual'
          },
          donor_information_auto_address_server_error: {
            pt: 'Ocorreu um erro ao recuperar os detalhes do endereço. Por favor, insira o endereço manualmente.',
            es: 'Hubo un error recuperando los datos de la dirección. Por favor ingresar la dirección manualmente.'
          },
          donor_information_auto_address_label: {
            pt: 'Buscar endereço',
            es: 'Buscar la Dirección'
          },
          donor_information_auto_address_place_holder: {
            pt: 'Insira o Endereço',
            es: 'Ingresa la Dirección'
          },
          donor_information_city: {
            pt: 'Cidade',
            es: 'Ciudad'
          },
          donor_information_state: {
            pt: 'Estado',
            es: 'Estado'
          },
          donor_information_postal_code: {
            pt: 'CEP',
            es: 'Código postal'
          },
          donor_information_postal_code_error: {
            pt: 'Erro no código postal',
            es: 'Error en el código postal'
          },
          donor_information_country: {
            pt: 'País',
            es: 'País'
          },
          donor_information_or_enter_postal_code: {
            pt: 'insira o código postal',
            es: 'o ingresa el código postal'
          },
          donor_information_heading: {
            pt: 'Suas informações',
            es: 'Tu información'
          },
          donor_information_billing: {
            pt: 'Endereço de cobrança',
            es: 'Domicilio de facturación'
          },
          donation_amount_text: {
            pt: 'Valor da doação',
            es: 'Monto del obsequio'
          },
          general_done_button: {
            pt: 'Feito',
            es: 'Hecho'
          },
          general_edit_button: {
            pt: 'Editar',
            es: 'Editar'
          },
          payments_total_payment_amount: {
            pt: 'Valor total do pagamento',
            es: 'Monto total'
          }
        }
      }
    };
  }

  static funraiseAware(f, u, n, r, a, scriptOnLoad) {
    const data = {
      window,
      document,
      tag: 'script',
      data: 'funraise',
      orgId: f,
      uri: u,
      common: n,
      client: r,
      script: a
    };

    data.window[data.data] = data.window[data.data] || [];

    if (data.window[data.data].scriptIsLoading || data.window[data.data].scriptIsLoaded) {
      return;
    }

    data.window[data.data].loading = true;
    data.window[data.data].push('init', data);
    const scripts = data.document.getElementsByTagName(data.tag)[0];
    const funraiseScript = data.document.createElement(data.tag);
    funraiseScript.async = true;
    funraiseScript.onload = scriptOnLoad;
    funraiseScript.src = data.uri + data.common + data.script + '?orgId=' + data.orgId;
    scripts.parentNode.insertBefore(funraiseScript, scripts);
  }

  static success(donation) {
    if (Array.isArray(window.dataLayer)) {
      window.dataLayer.push({
        event: 'donation:funraise',
        funraiseDonationData: donation
      });
    }
  }

  static initForms(formIds) {
    formIds.forEach((formId) => {
      window.funraise.push('create', {form: formId});

      if (window.funraise && window.funraise.push) {
        window.funraise.push('onSuccess', {form: formId}, (donor, donation) => {
          AEFunraise.success(donation);
        });

        window.funraise.push('config', {form: formId}, AEFunraise.getSettings());
      }
    });
  }
}

export default (selector) => {
  const formButtons = document.querySelectorAll(selector);
  const formsToInitialize = [];

  formButtons.forEach(formButton => {
    const formId = formButton.getAttribute('data-formId');

    if (formId && !isNaN(formId) && formsToInitialize.findIndex(form => form === formId) === -1) {
      formsToInitialize.push(formId);
    }
  });

  if (!window.funraiseOrgId || !formsToInitialize.length) {
    return;
  }

  AEFunraise.funraiseAware(
    window.funraiseOrgId,
    'https://assets.funraise.io',
    '/widget/common/2.0',
    '/widget/client',
    '/inject-form.js',
    () => AEFunraise.initForms(formsToInitialize)
  );
};
